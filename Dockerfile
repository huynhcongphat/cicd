FROM node:latest as build-phat
WORKDIR /app
COPY . .
RUN npm install 
RUN npm install react-scripts@3.0.1 -g
RUN npm rebuild node-sass
RUN npm run build

# production stage
FROM nginx:1.17-alpine as production-stage
COPY --from=build-phat /app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]